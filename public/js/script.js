// 
var lat;
var lng;
$(document).ready(geoLocationInit);
// 
function geoLocationInit() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, fail);
    } else {
        alert("Browser not supported. Upgrade your browser");
    }
}


var map;

function createMap(lat,lng){
    var userCoords=new google.maps.LatLng(lat,lng);
    //defining map options
    
    var mapOptions={
        center:userCoords,
        zoom:15
    }

     map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // *
    //  * Creating Marker at current location
     
    // custom image icon for marker
    // var imageIcon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
    var imageIcon="image/Hospital-icon.png";
    var marker = new google.maps.Marker({
        position: userCoords,
        map: map,
        title: 'Hello World!'
    });

   searchNearbyPlaces('Hospitals', userCoords, 1500, ['hospital'], imageIcon);
}

function success(position) {
    lat=position.coords.latitude;
    lng=position.coords.longitude;
    createMap(lat,lng);

    // getting current user lat lng value
    // var userCoords = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    // //defining map options
    // var mapOptions = {
    //         center: userCoords,
    //         zoom: 15
    //     }
    //     //creating map object and place it in div with id map
    // map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // /**
    //  * Creating Marker at current location
    //  */
    // // custom image icon for marker
    // var imageIcon = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
    // var marker = new google.maps.Marker({
    //     position: userCoords,
    //     map: map,
    //     title: 'Hello World!'
    // });

    /**
     * Nearby Places search
     */
    // calling function
    // searchNearbyPlaces('Schools', userCoords, 1500, ['school'], imageIcon);

}

function fail() {
    // alert("Error obtaining position");
    lat=27.6833;
    lng=84.4333;
    createMap(lat,lng);
}


function searchNearbyPlaces(title, latlng, radius, types, icon) {

    var request = {
        location: latlng,
        radius: radius,
        types: types,
    }
    var callPlaces = new google.maps.places.PlacesService(map);
    callPlaces.nearbySearch(request, callback);

    function callback(results, status) {
        //results contains several objects 
        $.each(results, function(i, place) {
            var placeLoc = place.geometry.location;
            var markPlace = new google.maps.Marker({
                position: placeLoc,
                map: map,
                icon: icon,
                title: place.name
            });
        });

    }



}