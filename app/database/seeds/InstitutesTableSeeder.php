<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class InstitutesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('ne_NP');
		Institute::truncate();

		for($i=0;$i<200;$i++)
		{
			Institute::create([
				'name'=> $faker->company,
				'description'=>$faker->realText(200,2),
				'district'=>$faker->district,
				'city'=>$faker->cityName,
				'lat'=>$faker->unique()->latitude(26,30),
				'lng'=>$faker->unique()->longitude(80,88),

			]);
		}
	}

}