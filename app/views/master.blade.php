<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Map</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('css/styles.css')}}">
	
	@yield('style')
</head>
<body>
<div class=" text-center container">
	
	@yield('content')
</div>
	<script src="{{asset('js/jquery.js')}}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXE9NTwCbgCG88wRt9ZG8OsyIShC7728k&libraries=places"
    async defer></script>
    <script src="{{asset('js/script.js')}}"></script>
    <script src="{{asset('js/ajaxsearch.js')}}"></script>
    
	@yield('script')
</body>
</html>