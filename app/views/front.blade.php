@extends('master')

@section('content')
	<h1>Google Api test</h1>
         <div id="map"></div>
<br><br>
         {{-- user current loaction hidden Form  --}}
         <div class="row text-left">
         	<div class="col-md-6 col-md-offset-3">
	         	{{ Form::open(['action'=>'SearchController@searchQuery','method'=>'post']) }}

		    	<div class="form-group">
		           {{ Form::label('district', 'Select District') }}
		           {{ Form::select('district', $districts,null, array('class' => 'form-control','id'=>'district')) }}
		       </div>
		        <div id="city" class="form-group">
		          {{--  {{ Form::label('city', 'Select City') }}
		           {{ Form::select('city', $cities,null, array('class' => 'form-control','id'=>'city')) }} --}}
		       </div>

			    {{ Form::submit('Find', array('class' => 'btn btn-primary')) }}

			    {{ Form::close() }}
		
			   
			    <div class="text-center" id="queryResults">

			
			<h3>Results</h3>
			<hr>
			   <ul>
   
		   @if(!empty($results))
			 	@foreach($results as $result)
			 			<p><strong>{{$result->name}}</strong></p>
						<li>{{$result->city}}</li>
						<li>{{$result->district}}</li>
			   	@endforeach

			@endif
			    	
			    	
			    </div>
         	</div>
         </div>
	 
   

@stop

