 @foreach ($errors->all() as $error)
    <li class="alert alert-warning">{{$error}}</li>
    
    @endforeach

       <div class="form-group">
           {{ Form::label('district', 'Select District') }}
           {{ Form::select('district', $districts, null, array('class' => 'form-control')) }}
       </div>
        <div class="form-group">
           {{ Form::label('city', 'Select City') }}
           {{ Form::select('city', $cities, null, array('class' => 'form-control')) }}
       </div>

