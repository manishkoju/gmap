<!DOCTYPE html>
<html>
  <head>
    <title>Admin Area</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="{{asset('admin/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- styles -->
    <link href="{{asset('admin/css/styles.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    {{-- header --}}
    @include('admin.layouts.include.header')

    <div class="page-content">
     {{-- Message --}}
        @if(Session::has('message'))
          <div class="alert alert-info">
              <p>{{ Session::get('message') }}</p>
          </div>
        @endif

      <div class="row">
      {{-- Admin side Nav Inculde  --}}

          @include('admin.layouts.include.sidenav')

          {{-- Admin content area --}}

      <div class="col-md-10 display-area">
        <div class="row text-center">
          <div class="col-md-10 col-md-offset-1">
            <div class="content-box-large">
                    {{-- yielding content here --}}
              @yield('content')
            </div>
          </div>
          
        </div>      

        
        </div><!--/Display area after sidenav-->
      </div>
    </div>
    </div><!--/Page Content-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/js/custom.js')}}"></script>
  </body>
</html>