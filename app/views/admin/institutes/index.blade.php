@extends('admin.layouts.master')

@section('content')
    <h4>Institutes Section </h4>
		
		<table class="table table-bordered ">
			    <thead>
			      <tr>
			        <th>ID</th>
			        <th>Name</th>
			        <th>District</th>
			        <th>City</th>
			        <th>Description</th>
			        <th>Latitude</th>
			        <th>Longitude</th>
			        <th>Action</th>
			        
			      </tr>
			    </thead>
			    <tbody>
			    @foreach($institutes as $i)
			    <tr>
			    	<td>{{$i->id}}</td>
			    	<td>{{$i->name}}</td>
			    	<td>{{$i->district}}</td>
			    	<td>{{$i->city}}</td>
			    	<td>{{$i->description}}</td>
			    	<td>{{$i->lat}}</td>
			    	<td>{{$i->lng}}</td
			    	<td>
			    	{{-- {{Form::open(array('route' =>['admin.institutes.destroy',$i->id])) }}
			    	{{Form::hidden('_method','DELETE')}}
			    	{{Form::submit('Delete',array('class'=>'btn btn-danger'))}}
			        {{Form::close ()}}
 --}}
			    	</td>
			    </tr>
			  @endforeach
			  
			    </tbody>
			     <thead>
			      <tr>
			        <th>ID</th>
			        <th>Name</th>
			        <th>District</th>
			        <th>City</th>
			        <th>Description</th>
			        <th>Latitude</th>
			        <th>Longitude</th>
			        <th>Action</th>
			        
			      </tr>
			      </thead>
  </table>

@stop 