{{-- slider form Section --}}

 <div class="text-left">      
 
 <div class="form-group">
 {{Form::label('name','Name')}}
 {{Form::text('name',null,array('class'=>'form-control'))}}
 </div>

 <div class="form-group">
{{Form::label('district','District')}}
{{Form::text('district',null,array('class'=>'form-control'))}}
</div>

 <div class="form-group">
 {{Form::label('city','City')}}
 {{Form::text('city',null,array('class'=>'form-control'))}}
 </div>

<div class="form-group">
 {{Form::label('description','Description')}}
 {{Form::text('description',null,array('class'=>'form-control'))}}	
</div>

<div class='form-group'>
{{Form::label('lat','Latitude')}}
{{Form::text('lat',null,array('class'=>'form-control'))}}
</div>

<div class='form-group'>
{{Form::label(lng','Longitude')}}
{{Form::text('lng',null,array('class'=>'form-control'))}}
</div>


</div>
 