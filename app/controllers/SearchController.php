<?php

class SearchController extends \BaseController {

	public function index(){
		$districts=Institute::lists('district','id');
		$cities=Institute::lists('city','id');
        // $names=Institute::lists('name','id');

		return Redirect::route('frontpage');

	}
	public function searchCity(){
		$distval=Input::get('distval');
		$matchedCities=Institute::where('district',$distval)->lists('city','city');
		return View::make('ajxresult',compact('matchedCities'));
		

	}
   //search to get result of insitutes by dropdown
	public function searchQuery(){
		$city= Input::get('city');
		// dd($city);
		$districts=Institute::lists('district','district');
		$results=Institute::where('city',$city)->get();
		return View::make('front',compact('results','districts'));
	}

}