<?php

class Institute extends Eloquent {

	
	public static $rules = [
		'name'=>'required',
		'district'=>'required',
		'city'=>'required',
		'description'=>'required',
		'lat'=>'required',
		'lng'=>'required',
	];

	
	protected $fillable = ['name','description','district','city','lat','lng'];

}