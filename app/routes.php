InstitutesController<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as'=>'frontpage',function()
{
	$districts=Institute::lists('district','district');
	return View::make('front',compact('districts','cities'));
}]);

Route::resource('admin/institutes','InstitutesController');
Route::get('/search', 'SearchController@index');
Route::post('/searchcity','SearchController@searchCity');
Route::post('/searchquery','SearchController@searchQuery');
